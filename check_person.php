<?php

/*
 * Following code will check if the personalid is already there
 *
 */

// array for JSON response
$response = array();

// check for required fields
if (isset($_GET["gid"])) {

    $gid = $_GET['gid'];

    // include db connect class
    require_once __DIR__ . '/db_connect.php';

    // connecting to db
    $db = new DB_CONNECT();

    // mysql inserting a new row
    $result = mysql_query("select * from person where gid = '$gid' ");

    if (!empty($result)){
        //Check for empty result
        if (mysql_num_rows($result) > 0) {
            $result = mysql_fetch_array($result);

            $myresults = array();
            $myresults["id"] = $result["id"];
            $myresults["name"] = $result["name"];
            $myresults["type"] = $result["type"];
            $myresults["gid"] = $result["gid"];
            $myresults["fname"] = $result["fname"];
            $myresults["lname"] = $result["lname"];

            //sucess
            $response["success"]=1;

            //user node
            $response["myresults"] = array();
            array_push($response["myresults"],$myresults);

            //echoing JSON response
            echo json_encode($response);
        } else {
            // no product found
            $response["success"] = 0;
            $response["message"] = "No product found";

            // echo no user JSON
            echo json_encode($response);
        }

    } else {
        // required field is missing
        $response["success"] = 0;
        $response["message"] = "Required field(s) is missing";

        // echoing JSON response
        echo json_encode($response);
    }
}
?>
